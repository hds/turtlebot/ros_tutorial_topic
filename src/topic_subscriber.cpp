// ROS header
#include <ros/ros.h>
#include "ros_tutorial_topic/MsgTutorial.h"

// Message callback function
void msgCallback(const ros_tutorial_topic::MsgTutorial::ConstPtr& msg) {
    ROS_INFO("receive msg = %d.%d : %d", msg->stamp.sec, msg->stamp.nsec, msg->data);
}


int main(int argc, char** argv) {

    ros::init(argc, argv, "topic_subscriber");
    ros::NodeHandle nh;

    ros::Subscriber ros_tutorial_sub = nh.subscribe("ros_tutorial_msg", 100, msgCallback);

    // Enter event loop
    ros::spin();

    return 0;
}