// Include ros default header file
#include <ros/ros.h>

// Include MsgTutorial message header (auto generated)
#include "ros_tutorial_topic/MsgTutorial.h"

int main(int argc, char** argv) {
    ros::init(argc, argv, "topic_publisher"); // Node name
    ros::NodeHandle nh; // Node handle for communication with the system

    /* Publisher declaration
    - Name "ros_tutorial_pub"
    - Using MsgTutorial from "ros_tutorial_topic" package
    - Topic name "ros_tutorial_msg"
    - Publisher queue 100
    */
   ros::Publisher ros_tutorial_pub = nh.advertise<ros_tutorial_topic::MsgTutorial>("ros_tutorial_msg", 100);

   // Set the loop period (in Hz)
   ros::Rate loop_rate(10);
   
   // Declate message
   ros_tutorial_topic::MsgTutorial msg;

   int count = 0;

   while (ros::ok()) {
       msg.stamp = ros::Time::now();
       msg.data = count;

       ROS_INFO("send msg = %d.%d : %d", msg.stamp.sec, msg.stamp.nsec, msg.data);
       ros_tutorial_pub.publish(msg);

       loop_rate.sleep();

       ++count;
   }

   return 0;
   
}